
CC = g++
CFLAGS = -g -Wall
TARGET = main

all: $(TARGET)

$(TARGET): $(TARGET).cpp
	$(CC) $(FLAGS) -o $(TARGET) $(TARGET).cpp -lsfml-graphics -lsfml-window -lsfml-system

run:
	./$(TARGET)

clean:
	$(RM) $(TARGET)
